
if SERVER then
	local function InitializeTomasPrinters()
		
		TPRINTERS_CONFIG = {}
		
		TPRINTERS_CONFIG.BatteryAdd = 50 -- How much battery entity adds battery to the printer.

		TPRINTERS_CONFIG.CoolingCellAdd = 50 -- How much cooling cell entity adds cooling to the printer.

		------------------------------------------BLUE PRINTER----------------------------------------------------------

		TPRINTERS_CONFIG.Name = "Money Printer" -- Printer name
		
		TPRINTERS_CONFIG.Battery = 10 -- How much battery does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Cooling = 10 -- How much cooling gel does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Heat = 20 -- How much heat does the printer get if the cooling cell is empty
		
		TPRINTERS_CONFIG.Money = 5 -- How much money does it prints. This number is multiplied by the printer speed (stars).
		
		TPRINTERS_CONFIG.PrintRate = math.random(30,60) -- That will give random number between 30 and 60 for the print rate. Time is in seconds.

		TPRINTERS_CONFIG.UpgradePrice = 60 -- This is the upgrade price of the printer for 1 star
		
		------------------------------------------BLUE PRINTER----------------------------------------------------------
		
		------------------------------------------RED PRINTER----------------------------------------------------------

		TPRINTERS_CONFIG.Name_Red = "Money Printer" -- Printer name
		
		TPRINTERS_CONFIG.Battery_Red = 10 -- How much battery does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Cooling_Red = 10 -- How much cooling gel does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Heat_Red = 20 -- How much heat does the printer get if the cooling cell is empty
		
		TPRINTERS_CONFIG.Money_Red = 10 -- How much money does it prints. This number is multiplied by the printer speed (stars).
		
		TPRINTERS_CONFIG.PrintRate_Red = math.random(30,60) -- That will give random number between 30 and 60 for the print rate. Time is in seconds.

		TPRINTERS_CONFIG.UpgradePrice_Red = 90 -- This is the upgrade price of the printer for 1 star
		
		------------------------------------------RED PRINTER----------------------------------------------------------
		
		------------------------------------------GREEN PRINTER----------------------------------------------------------

		TPRINTERS_CONFIG.Name_Green = "Money Printer" -- Printer name
		
		TPRINTERS_CONFIG.Battery_Green = 10 -- How much battery does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Cooling_Green = 10 -- How much cooling gel does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Heat_Green = 20 -- How much heat does the printer get if the cooling cell is empty
		
		TPRINTERS_CONFIG.Money_Green = 15 -- How much money does it prints. This number is multiplied by the printer speed (stars).
		
		TPRINTERS_CONFIG.PrintRate_Green = math.random(30,60) -- That will give random number between 30 and 60 for the print rate. Time is in seconds.

		TPRINTERS_CONFIG.UpgradePrice_Green = 120 -- This is the upgrade price of the printer for 1 star
		
		------------------------------------------GREEN PRINTER----------------------------------------------------------

		------------------------------------------YELLOW PRINTER----------------------------------------------------------

		TPRINTERS_CONFIG.Name_Yellow = "Money Printer" -- Printer name
		
		TPRINTERS_CONFIG.Battery_Yellow = 10 -- How much battery does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Cooling_Yellow = 10 -- How much cooling gel does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Heat_Yellow = 20 -- How much heat does the printer get if the cooling cell is empty
		
		TPRINTERS_CONFIG.Money_Yellow = 20 -- How much money does it prints. This number is multiplied by the printer speed (stars).
		
		TPRINTERS_CONFIG.PrintRate_Yellow = math.random(30,60) -- That will give random number between 30 and 60 for the print rate. Time is in seconds.

		TPRINTERS_CONFIG.UpgradePrice_Yellow = 150 -- This is the upgrade price of the printer for 1 star
		
		------------------------------------------YELLOW PRINTER----------------------------------------------------------

		------------------------------------------PURPLE PRINTER----------------------------------------------------------

		TPRINTERS_CONFIG.Name_Purple = "Money Printer" -- Printer name
		
		TPRINTERS_CONFIG.Battery_Purple = 10 -- How much battery does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Cooling_Purple = 10 -- How much cooling gel does it take from the printer when it prints something
		
		TPRINTERS_CONFIG.Heat_Purple = 20 -- How much heat does the printer get if the cooling cell is empty
		
		TPRINTERS_CONFIG.Money_Purple = 40 -- How much money does it prints. This number is multiplied by the printer speed (stars).
		
		TPRINTERS_CONFIG.PrintRate_Purple = math.random(30,60) -- That will give random number between 30 and 60 for the print rate. Time is in seconds.

		TPRINTERS_CONFIG.UpgradePrice_Purple = 180 -- This is the upgrade price of the printer for 1 star
		
		------------------------------------------PURPLE PRINTER----------------------------------------------------------

	end
	
	hook.Add("Initialize","InitializeTomasPrinters",InitializeTomasPrinters)

end
