/*---------------------------------------------------
Name: Anti-Prop Surf (gm_surf)
Version: 1.11
Author: Irkalla
Purpose: Disables collisions with an entity
and all constrained entities and a player
once the player picks up the entity with
a physics gun, effectively removing a
player's ability to prop surf.
----------------------------------------------------*/

if not SERVER then return end 

CreateConVar( "sv_allow_prop_surf", "0", { FCVAR_REPLICATED, FCVAR_NOTIFY } )

hook.Add(  "OnPhysgunFreeze", "gm_surf_freeze", function( weapon, physobj, ent, ply ) 
	local CanSurf = GetConVar( "sv_allow_prop_surf" ):GetBool()
	local ConstrainedEnts = constraint.GetAllConstrainedEntities( ent )
	
	if !CanSurf then
		if ( ent:IsValid() && 
		ent:IsVehicle() ) then
			ent:SetCollisionGroup( COLLISION_GROUP_VEHICLE )
			ent:SetPos( ent:GetPos() )
		elseif ( ent:IsValid() &&
		!ent:IsPlayer() && 
		!ent:IsNPC() &&
		!ent:IsWeapon() &&
		!ent:IsVehicle() ) then
			ent:SetCollisionGroup( COLLISION_GROUP_NONE )
			ent:SetPos( ent:GetPos() )
		end
	 
		ply:SetCollisionGroup( COLLISION_GROUP_PLAYER )
		ply:SetPos( ply:GetPos() )
	 
		for _, ent in pairs( ConstrainedEnts ) do
			if ent:IsValid() then
				if ent:IsVehicle() then
					ent:SetCollisionGroup( COLLISION_GROUP_VEHICLE )
					ent:SetPos( ent:GetPos() )
				elseif ( !ent:IsPlayer() &&
				!ent:IsWeapon() &&
				!ent:IsNPC() &&
				!ent:IsVehicle() ) then
					ent:SetCollisionGroup( COLLISION_GROUP_NONE )
					ent:SetPos( ent:GetPos() )
				end
		     
			end
		  
		end
	 
	end
	
end )

hook.Add( "PhysgunPickup", "gm_surf_pickup", function( ply, ent )
	local CanSurf = GetConVar( "sv_allow_prop_surf" ):GetBool()
	local ConstrainedEnts = constraint.GetAllConstrainedEntities( ent )
	
	if !CanSurf then
		if ( ent:IsValid() &&
		!ent:IsPlayer() &&
		!ent:IsWeapon() &&
		!ent:IsNPC() ) then
			ply:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE_DEBRIS )
			ent:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE_DEBRIS )
			ply:SetPos( ply:GetPos() )
			ent:SetPos( ent:GetPos() )
		end
		
	 
		for _, ent in pairs( ConstrainedEnts ) do
			if ent:IsValid() then
				if !ent:IsPlayer() &&
				!ent:IsWeapon() &&
				!ent:IsNPC() then
					ent:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE_DEBRIS )
					ent:SetPos( ent:GetPos() )
				end
		     
			end
		 
		end
	 
	end
	
end )

hook.Add( "PhysgunDrop", "gm_surf_drop", function( ply, ent )
	local CanSurf = GetConVar( "sv_allow_prop_surf" ):GetBool()
	local ConstrainedEnts = constraint.GetAllConstrainedEntities( ent )
	
	if !CanSurf then
		if ( ent:IsValid() &&
		ent:IsVehicle() ) then
			ent:SetCollisionGroup( COLLISION_GROUP_VEHICLE )
			ent:SetPos( ent:GetPos() )
		elseif ( ent:IsValid() &&
		!ent:IsWeapon() &&
		!ent:IsPlayer() &&
		!ent:IsNPC() &&
		!ent:IsVehicle() ) then
			ent:SetCollisionGroup( COLLISION_GROUP_NONE )
			ent:SetPos( ent:GetPos() )
		end
	 
		ply:SetCollisionGroup( COLLISION_GROUP_PLAYER )
		ply:SetPos( ply:GetPos() )
	 
		for _, ent in pairs( ConstrainedEnts ) do
			if ent:IsValid() then
				if ent:IsVehicle() then
					ent:SetCollisionGroup( COLLISION_GROUP_VEHICLE )
					ent:SetPos( ent:GetPos() )
				elseif ( !ent:IsPlayer() &&
				!ent:IsWeapon() &&
				!ent:IsNPC() &&
				!ent:IsVehicle() ) then
					ent:SetCollisionGroup( COLLISION_GROUP_NONE )
					ent:SetPos( ent:GetPos() )
				end
				
			end
			
		end
		
	end
	
end )