/*---------------------------------------------------------------------------
	
	Creator: TheCodingBeast - TheCodingBeast.com
	This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. 
	To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
	
---------------------------------------------------------------------------*/

-- Variables
TCB_Settings = {}

-- Settings
TCB_Settings.ActivationKey1 = "ShowSpare2"		// F1 (ShowHelp), 	F2 (ShowTeam), 	F3 (ShowSpare1), 	F4 (ShowSpare2)
TCB_Settings.ActivationKey2	= KEY_F4			// F1 (KEY_F1), 	F2 (KEY_F2), 	F3 (KEY_F3), 		F4 (KEY_F4)

TCB_Settings.CheckVersion	= true

TCB_Settings.HideWrongJob	= true

TCB_Settings.TitleOne		= "ClassicRP"
TCB_Settings.TitleTwo		= "Концерты в 18:00 мо МСК!"

TCB_Settings.PrimaryColor	= Color( 52, 152, 219, 255 )
TCB_Settings.SecondaryColor	= Color( 41, 128, 185, 255 )

-- Custom Web Panels (If Enabled Below)
TCB_Settings.WebPanel_1		= "https://paksol.ru"
TCB_Settings.WebPanel_2		= "https://paksol.ru"
TCB_Settings.WebPanel_3		= "https://paksol.ru"
TCB_Settings.WebPanel_4		= "https://paksol.ru" 

-- Buttons
TCB_Settings.SidebarButtons = {
	
	{ text = "Команды", 	panel = "tcb_panel_commands", 	info = true, 	func = 6			},

	{ text = "Divider",		panel = "",						info = false,	func = 0 			},

	{ text = "Форум",		panel = "tcb_panel_custom1",	info = false,	func = 0 			},
	{ text = "Правила",		panel = "tcb_panel_custom2",	info = false,	func = 0 			},
	{ text = "Магазин",		panel = "tcb_panel_custom3",	info = false,	func = 0 			},
	{ text = "Разное",		panel = "tcb_panel_custom4",	info = false,	func = 0 			},

	{ text = "Divider",		panel = "",						info = false,	func = 0 			},

	{ text = "Работы", 		panel = "tcb_panel_jobs",		info = true,	func = "jobs"  		},
	{ text = "Вещи",	panel = "tcb_panel_entities",	info = true,	func = "entities"	},
	{ text = "Оружие",		panel = "tcb_panel_guns",		info = true,	func = "weapons"	},
	{ text = "Поставки",	panel = "tcb_panel_shipments",	info = true,	func = "shipments" 	},
	{ text = "Патроны",		panel = "tcb_panel_ammo",		info = true,	func = "ammo" 		},
	{ text = "Машины{скоро!}",	panel = "tcb_panel_vehicles",	info = true,	func = "vehicles"	},

}

-- Version (Don't Change)
TCB_Settings.Version 		= "1.8"