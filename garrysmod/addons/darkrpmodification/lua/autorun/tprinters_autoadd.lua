
timer.Simple(4, function()

	DarkRP.createEntity("Синий (1-ый ур.)", {
		ent = "boost_printer",
		model = "models/props_c17/consolebox01a.mdl",
		price = 300,
		max = 2,
		cmd = "buyblueprinter",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})

	DarkRP.createEntity("Красный (2-ой ур.)", {
		ent = "boost_printer_red",
		model = "models/props_c17/consolebox01a.mdl",
		price = 500,
		max = 2,
		cmd = "buyredprinter",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})

	DarkRP.createEntity("Зелёный (3-ий ур.)", {
		ent = "boost_printer_green",
		model = "models/props_c17/consolebox01a.mdl",
		price = 750,
		max = 2,
		cmd = "buygreenprinter",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})
	
	DarkRP.createEntity("Желтый (4-ый ур.)", {
		ent = "boost_printer_yellow",
		model = "models/props_c17/consolebox01a.mdl",
		price = 1000,
		max = 2,
		cmd = "buyyellowprinter",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})

	DarkRP.createEntity("Админ принтер (5-ый ур.)", {
		ent = "boost_printer_purple",
		model = "models/props_c17/consolebox01a.mdl",
		price = 1500,
		max = 2,
		cmd = "buypurpleprinter",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})
	DarkRP.createEntity("Охлаждающая ячейка", {
		ent = "boost_cooling",
		model = "models/Items/battery.mdl",
		price = 150,
		max = 1,
		cmd = "buycoolingcell",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})

	DarkRP.createEntity("Батарея для принтера", {
		ent = "boost_battery",
		model = "models/Items/car_battery01.mdl",
		price = 100,
		max = 1,
		cmd = "buybattery",
		allowed = {TEAM_GANG, TEAM_MAFIAMAN, TEAM_BOSS_JN , TEAM_BOSS , TEAM_BANK }
	})

end)
