/*----------------------------------------------------------------------
Leak by Famouse

Play good games:↓
http://store.steampowered.com/curator/32364216

Subscribe to the channel:↓
https://www.youtube.com/c/Famouse

More leaks in the discord:↓ 
discord.gg/rFdQwzm
------------------------------------------------------------------------*/
include("shared.lua")
AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

util.AddNetworkString( "UpdatePrinter" )

local function UpdatePrinter(ent)
	local Tabl = {}
	Tabl.Battery = ent.Battery
	Tabl.Heat = ent.Heat
	Tabl.Speed = ent.Speed
	Tabl.PrintedMoney = ent.PrintedMoney
	Tabl.Cooling = ent.Cooling
	Tabl.PrintRate = ent.Speed*TPRINTERS_CONFIG.Money_Yellow
	Tabl.Name = TPRINTERS_CONFIG.Name_Yellow
	
	net.Start("UpdatePrinter")
		net.WriteTable(Tabl)
		net.WriteEntity(ent)
	net.Broadcast()
end

local function MinTimer(ent)
	if not ent:IsValid() then return end
	if ent.Battery > 0 then
		ent.PrintedMoney = ent.PrintedMoney + ent.Speed * TPRINTERS_CONFIG.Money_Yellow
		
		ent.Battery = ent.Battery - TPRINTERS_CONFIG.Battery_Yellow
		if ent.Battery < 0 then ent.Battery = 0 end
		
		ent.Cooling = ent.Cooling - TPRINTERS_CONFIG.Cooling_Yellow
		if ent.Cooling < 0 then ent.Cooling = 0 end
		
		if ent.Cooling > 0 then
			ent.Heat = ent.Heat - 5 
		else
			ent.Heat = ent.Heat + TPRINTERS_CONFIG.Heat_Yellow 
		end
		if ent.Heat > 100 then ent.Heat = 100 end
		if ent.Heat < 10 then ent.Heat = 10 end
	else
		ent.Heat = ent.Heat - 20
		if ent.Heat < 0 then ent.Heat = 0 end
	end
	
	if ent.Heat >= 100 then
		ent:Destruct()
		ent:Remove()
	end
	UpdatePrinter(ent)
	timer.Simple(TPRINTERS_CONFIG.PrintRate_Yellow, function() MinTimer(ent) end)
end

function ENT:Destruct()
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart(vPoint)
	effectdata:SetOrigin(vPoint)
	effectdata:SetScale(1)
	util.Effect("Explosion", effectdata)
	DarkRP.notify(self:Getowning_ent(), 1, 4, DarkRP.getPhrase("money_printer_exploded"))
end

function ENT:OnTakeDamage(dmg)
	if self.burningup then return end

	self.damage = (self.damage or 100) - dmg:GetDamage()
	if self.damage <= 0 then
		self:Destruct()
		self:Remove()
	end
end


function ENT:Initialize()
	self:SetModel("models/props_c17/consolebox01a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()
	phys:Wake()
	self:SetUseType( SIMPLE_USE )
	
	self.Battery = 100
	self.Heat = 10
	self.Speed = 1
	self.PrintedMoney = 0
	self.Cooling = 100
	timer.Simple(1,function() if self:IsValid() then UpdatePrinter(self) end end)
	timer.Simple(TPRINTERS_CONFIG.PrintRate_Yellow, function() MinTimer(self) end)
	self.sound = CreateSound(self, Sound("ambient/levels/labs/equipment_printer_loop1.wav"))
	self.sound:SetSoundLevel(52)
	self.sound:PlayEx(1, 100)
end


local function WorldToScreen(vWorldPos,vPos,vScale,aRot)
    local vWorldPos=vWorldPos-vPos
    vWorldPos:Rotate(Angle(0,-aRot.y,0))
    vWorldPos:Rotate(Angle(-aRot.p,0,0))
    vWorldPos:Rotate(Angle(0,0,-aRot.r))
    return vWorldPos.x/vScale,(-vWorldPos.y)/vScale
end

local function inrange(x, y, x2, y2, x3, y3)
	if x > x3 then return false end
	if y < y3 then return false end
	if x2 < x3 then return false end
	if y2 > y3 then return false end
	return true
end

function ENT:OnRemove()
	if self.sound then
		self.sound:Stop()
	end
end

function ENT:Use(ply) 
	if(ply:IsPlayer())then 
		local lookAtX,lookAtY = WorldToScreen(ply:GetEyeTrace().HitPos or Vector(0,0,0),self:GetPos()+self:GetAngles():Up()*1.55, 0.2375, self:GetAngles())
		if inrange(-31, -34, -17, -55, lookAtX, lookAtY) and ply:getDarkRPVar("money") >= TPRINTERS_CONFIG.UpgradePrice_Yellow and self.Speed < 9 then
			sound.Play( "buttons/button15.wav", self:GetPos(), 100, 100, 1 )
			ply:addMoney( -TPRINTERS_CONFIG.UpgradePrice_Yellow )
			self.Speed = self.Speed + 1
			DarkRP.notify( ply, 1, 5, "Printer speed has been upgraded!" )
			UpdatePrinter(self)
		elseif self.PrintedMoney > 0 then
			ply:addMoney( self.PrintedMoney )
			DarkRP.notify( ply, 0, 5, "You picked up $"..self.PrintedMoney )
			self.PrintedMoney = 0
			UpdatePrinter(self)
		end
    end
end

function ENT:UpdatePrintera()
	UpdatePrinter(self)
end


function ENT:Think()
	if not self:Getowning_ent():IsValid() then
		self:Remove()
	end
end
/*------------------------------------------------------------------------
Donation for leaks

Qiwi Wallet         4890494419811120 
YandexMoney         410013095053302
WebMoney(WMR)       R235985364414
WebMoney(WMZ)       Z309855690994
------------------------------------------------------------------------*/