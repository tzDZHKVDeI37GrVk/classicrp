--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------
This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.

Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
      Once you've done that, copy and paste the job to this file and edit it.

The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua

For examples and explanation please visit this wiki page:
https://darkrp.miraheze.org/wiki/DarkRP:CustomJobFields

Add your custom jobs under the following line:
---------------------------------------------------------------------------]]

--[[ standard citizen: 
"gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" 

standard police: 
"darkrp_handcuffs", "unarrest_stick", "fas2_glock20", "weapon_stunstick", "door_ram", "weaponchecker" 

locker : "lockpick"

глок - fas2_glock20

]]-- 
--Полицейский
TEAM_POLICE = DarkRP.createJob("Полицейский", {
    color = Color(46,27,253),
    model = {
        "models/player/kerry/thalia_sheriff_02.mdl",
		"models/player/kerry/f_sheriff_04.mdl",
		"models/player/kerry/f_sheriff_03.mdl",

        "models/kerry/player/police_usa/male_01.mdl",
        "models/kerry/player/police_usa/male_07.mdl",
        "models/kerry/player/police_usa/male_09.mdl",
        "models/kerry/player/police_usa/male_06.mdl",
        "models/kerry/player/police_usa/male_05.mdl",
		
    },
    description = [[Станьте рядовым полицейским]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" , "darkrp_handcuffs", "unarrest_stick", "weapon_stunstick", "door_ram", "weaponchecker" , "fas2_glock20"},
    command = "police",
    max = 0, 
    salary = 300,
    admin = 0,
    vote = false,
    hasLicense = true,
    category = "PROTECTORS", -- The name of the category it is in. Note: the category must be created!
    sortOrder = 100, -- The position of this thing in its category. Lower number means higher up.
    buttonColor = Color(255, 255, 255, 255), -- The color of the button in the F4 menu
    label = "Полицейский", -- Optional: the text on the button in the F4 menu
})

--Штурмовик
TEAM_SHTURM = DarkRP.createJob("Штурмовик", {
    color = Color(46,27,253),
    model = {
        "models/player/combine_soldier.mdl"
		
    },
    description = [[Повышение звания полицейского]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" , "darkrp_handcuffs", "unarrest_stick", "weapon_stunstick", "door_ram", "weaponchecker" , "fas2_glock20" , "fas2_sg552" },
    command = "shturm",
    max = 4, 
    salary = 500,
    admin = 0,
    vote = false,
    hasLicense = true,

    -- The following fields are OPTIONAL. If you do not need them, or do not need to change them from their defaults, REMOVE them.
    NeedToChangeFrom = TEAM_POLICE,
    category = "PROTECTORS", -- The name of the category it is in. Note: the category must be created!
    label = "Штурмовик", -- Optional: the text on the button in the F4 menu
})

--ОФИЦЕР
TEAM_OFFICER = DarkRP.createJob("Офицер", {
    color = Color(46,27,253),
    model = {
        "models/portal2/patrol_07.mdl",
        "models/portal2/patrol_06.mdl",
        "models/portal2/patrol_05.mdl",
        "models/portal2/patrol_04.mdl",
        "models/portal2/patrol_03.mdl",
        "models/portal2/patrol_09.mdl",
        "models/portal2/patrol_08.mdl",
    },
    description = [[Теперь вы офицер. Ура]],
    weapons = {"gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" , "darkrp_handcuffs", "unarrest_stick", "weapon_stunstick", "door_ram", "weaponchecker" , "fas2_glock20", "fas2_mp5a5"},
    command = "officer",
    max = 2, 
    salary = 800,
    admin = 0,
    vote = false,
    hasLicense = true,

    -- The following fields are OPTIONAL. If you do not need them, or do not need to change them from their defaults, REMOVE them.
    NeedToChangeFrom = TEAM_SHTURM,
    category = "PROTECTORS", -- The name of the category it is in. Note: the category must be created!
    label = "Офицер полиции", -- Optional: the text on the button in the F4 menu
})

--Шериф
TEAM_SHERIFF = DarkRP.createJob("Шериф города", {
    color = Color(46,27,253),
    model = {
        "models/gta5/lssd/sheriffg.mdl"
    },
    description = [[Вы главный у полиции]],
    weapons = {"gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" , "darkrp_handcuffs", "unarrest_stick", "weapon_stunstick", "door_ram", "weaponchecker", "fas2_ragingbull"},
    command = "sheriff",
    max = 1, 
    salary = 1500,
    admin = 0,
    vote = false,
    hasLicense = true,

    -- The following fields are OPTIONAL. If you do not need them, or do not need to change them from their defaults, REMOVE them.
    NeedToChangeFrom = TEAM_OFFICER,
    category = "PROTECTORS", -- The name of the category it is in. Note: the category must be created!
    label = "Шериф города", -- Optional: the text on the button in the F4 menu
})

--МАФИОЗНИК
TEAM_MAFIAMAN = DarkRP.createJob("Мафиозник", {
    color = Color(46,27,253),
    model = {
        "models/sentry/sentryoldmob/mafia/sentrymobmale2pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale9pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale8pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale7pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale6pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale5pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale4pm.mdl",
        "models/sentry/sentryoldmob/mafia/sentrymobmale3pm.mdl",
    },
    description = [[Вы организованный бандит]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys"  },
    command = "mafiaman",
    max = 4, 
    salary = 350,
    admin = 0,
    vote = false,
    hasLicense = true,

    -- The following fields are OPTIONAL. If you do not need them, or do not need to change them from their defaults, REMOVE them.
    NeedToChangeFrom = TEAM_GANG,
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Мафиозник", -- Optional: the text on the button in the F4 menu
})

--БОСИК
TEAM_BOSS_JN = DarkRP.createJob("Младший босс", {
    color = Color(46,27,253),
    model = {
        "models/sentry/sentryoldmob/oldgoons/sentryarmbmale6pm.mdl",
        "models/sentry/sentryoldmob/oldgoons/sentryarmbmale8pm.mdl",
        "models/sentry/sentryoldmob/oldgoons/sentryarmbmale2pm.mdl",
    },
    description = [[Вы организованный бандит]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys"  },
    command = "bossjn",
    max = 2, 
    salary = 550,
    admin = 0,
    vote = false,
    hasLicense = true,

    -- The following fields are OPTIONAL. If you do not need them, or do not need to change them from their defaults, REMOVE them.
    NeedToChangeFrom = TEAM_MAFIAMAN,
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Младший босс", -- Optional: the text on the button in the F4 menu
})

TEAM_BOSS = DarkRP.createJob("Босс", {
    color = Color(46,27,253),
    model = {
        "models/vito.mdl",
    },
    description = [[Вы глава криминала]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys"  },
    command = "boss",
    max = 1, 
    salary = 1200,
    admin = 0,
    vote = false,
    hasLicense = true,

    -- The following fields are OPTIONAL. If you do not need them, or do not need to change them from their defaults, REMOVE them.
    NeedToChangeFrom = TEAM_BOSS_JN,
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Босс", -- Optional: the text on the button in the F4 menu
})

--Взломщик
TEAM_LOCK = DarkRP.createJob("Взломщик", {
    color = Color(46,27,253),
    model = {
        "models/player/arctic.mdl",
    },
    description = [[Вы взламываете замки и кейпады]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" , "lockpick" , "keypad_cracker"},
    command = "lock",
    max = 2, 
    salary = 300,
    admin = 0,
    vote = false,
    hasLicense = true,

    
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Взломщик", -- Optional: the text on the button in the F4 menu
})


--Взломщик
TEAM_BANK = DarkRP.createJob("Банкир", {
    color = Color(46,27,253),
    model = {
        "models/player/hostage/hostage_04.mdl",
    },
    description = [[Вы печатаете деньги]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" },
    command = "bank",
    max = 2, 
    salary = 500,
    admin = 0,
    vote = false,
    hasLicense = true,

    category = "Citizens", -- The name of the category it is in. Note: the category must be created!
    label = "Взломщик", -- Optional: the text on the button in the F4 menu
})

--Маньяк
TEAM_MANIAC = DarkRP.createJob("Маньяк", {
    color = Color(46,27,253),
    model = {
        "models/player/soldier_stripped.mdl",
    },
    description = [[Вы всех убиваете ножом]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys", "weapon_crowbar" },
    command = "maniac",
    max = 1, 
    salary = 200,
    admin = 0,
    vote = false,
    hasLicense = true,

    
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Маньяк", -- Optional: the text on the button in the F4 menu
})

--Паркурист
TEAM_PARCOUR = DarkRP.createJob("Паркурист [VIP]", {
    color = Color(95,77,53),
    model = {
        "models/Barney.mdl",
    },
    description = [[Вы быстрый и ловкий]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys", "parkourmod" },
    command = "parkur",
    max = 1, 
    salary = 300,
    admin = 1,
    vote = false,
    hasLicense = false,
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Паркурист", -- Optional: the text on the button in the F4 menu
})

--Админ на службе
TEAM_ADMIN = DarkRP.createJob("Админ на службе", {
    color = Color(255,1,1),
    model = {
        "models/halo4/spartans/masterchief_player.mdl",
    },
    description = [[Вы следите за порядком]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" },
    command = "admin",
    max = 2, 
    salary = 300,
    admin = 2,
    vote = false,
    hasLicense = false,
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Админ", -- Optional: the text on the button in the F4 menu
})


--Админ на службе
TEAM_CHICKEN = DarkRP.createJob("Курочка [VIP]", {
    color = Color(255,1,1),
    model = {
        "models/player/chicken.mdl",
    },
    description = [[Надоедливая курочка]],
    weapons = { "gmod_tool", "weapon_fists" ,"weapon_physgun" , "weapon_physcannon" , "pocket", "keys" },
    command = "chicken",
    max = 2, 
    salary = 500,
    admin = 2,
    vote = false,
    hasLicense = false,
    category = "CRIMINALS", -- The name of the category it is in. Note: the category must be created!
    label = "Курочка", -- Optional: the text on the button in the F4 menu
})


--[[---------------------------------------------------------------------------
Define which team joining players spawn into and what team you change to if demoted
---------------------------------------------------------------------------]]
GAMEMODE.DefaultTeam = TEAM_CITIZEN
--[[---------------------------------------------------------------------------
Define which teams belong to civil protection
Civil protection can set warrants, make people wanted and do some other police related things
---------------------------------------------------------------------------]]
GAMEMODE.CivilProtection = {
    [TEAM_POLICE] = true,
    [TEAM_SHTURM] = true,
    [TEAM_OFFICER] = true,
    [TEAM_SHERIFF] = true,
    [TEAM_MAYOR] = true,
}
--[[---------------------------------------------------------------------------
Jobs that are hitmen (enables the hitman menu)
---------------------------------------------------------------------------]]
DarkRP.addHitmanTeam(TEAM_MANIAC)

