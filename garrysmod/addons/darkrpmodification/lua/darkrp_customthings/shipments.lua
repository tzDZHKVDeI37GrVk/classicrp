--[[---------------------------------------------------------------------------
DarkRP custom shipments and guns
---------------------------------------------------------------------------

This file contains your custom shipments and guns.
This file should also contain shipments and guns from DarkRP that you edited.

Note: If you want to edit a default DarkRP shipment, first disable it in darkrp_config/disabled_defaults.lua
    Once you've done that, copy and paste the shipment to this file and edit it.

The default shipments and guns can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua

For examples and explanation please visit this wiki page:
https://darkrp.miraheze.org/wiki/DarkRP:CustomShipmentFields


Add shipments and guns under the following line:
---------------------------------------------------------------------------]]

DarkRP.createShipment("АК-47", {
    model = "models/weapons/w_rif_ak47.mdl", -- The model of the item that hovers above the shipment
    entity = "fas2_ak47", -- the entity that comes out of the shipment
    price = 32000, -- the price of one shipment
    amount = 10, -- how many of the item go in one purchased shipment
    separate = true, -- whether the item is sold separately (usually used for guns)
    pricesep = 4000, -- the price of a separately sold item
    allowed = {TEAM_GUN}, -- OPTIONAL, which teams are allowed to buy this shipment/separate gun
    label = "АК-47", -- Optional: the text on the button in the F4 menu, if you want it different than the name above
    buttonColor = Color(224, 227, 34, 255), -- Optional: The color of the button in the F4 menu
}) 


DarkRP.createShipment("Glock-42", {
    model = "models/weapons/w_pist_glock18.mdl", -- The model of the item that hovers above the shipment
    entity = "fas2_glock20", -- the entity that comes out of the shipment
    price = 5600, -- the price of one shipment
    amount = 10, -- how many of the item go in one purchased shipment
    separate = true, -- whether the item is sold separately (usually used for guns)
    pricesep = 700, -- the price of a separately sold item
    allowed = {TEAM_GUN}, -- OPTIONAL, which teams are allowed to buy this shipment/separate gun
    label = "Glock 42", -- Optional: the text on the button in the F4 menu, if you want it different than the name above
    buttonColor = Color(224, 227, 34, 255), -- Optional: The color of the button in the F4 menu
}) 

DarkRP.createShipment("М24", {
    model = "models/weapons/w_snip_awp.mdl", -- The model of the item that hovers above the shipment
    entity = "fas2_m24", -- the entity that comes out of the shipment
    price = 96000, -- the price of one shipment
    amount = 10, -- how many of the item go in one purchased shipment
    separate = true, -- whether the item is sold separately (usually used for guns)
    pricesep = 12000, -- the price of a separately sold item
    allowed = {TEAM_GUN}, -- OPTIONAL, which teams are allowed to buy this shipment/separate gun
    label = "М24 снайперка", -- Optional: the text on the button in the F4 menu, if you want it different than the name above
    buttonColor = Color(224, 227, 34, 255), -- Optional: The color of the button in the F4 menu
}) 

DarkRP.createShipment("М3 дробовик", {
    model = "models/weapons/w_shot_m3super90.mdl", -- The model of the item that hovers above the shipment
    entity = "fas2_m3s90", -- the entity that comes out of the shipment
    price = 28000, -- the price of one shipment
    amount = 10, -- how many of the item go in one purchased shipment
    separate = true, -- whether the item is sold separately (usually used for guns)
    pricesep = 3500, -- the price of a separately sold item
    allowed = {TEAM_GUN}, -- OPTIONAL, which teams are allowed to buy this shipment/separate gun
    label = "М3 дробовик", -- Optional: the text on the button in the F4 menu, if you want it different than the name above
    buttonColor = Color(224, 227, 34, 255), -- Optional: The color of the button in the F4 menu
}) 

DarkRP.createShipment("МП5", {
    model = "models/weapons/w_smg_mp5.mdl", -- The model of the item that hovers above the shipment
    entity = "fas2_mp5a5", -- the entity that comes out of the shipment
    price = 16000, -- the price of one shipment
    amount = 10, -- how many of the item go in one purchased shipment
    separate = true, -- whether the item is sold separately (usually used for guns)
    pricesep = 2000, -- the price of a separately sold item
    allowed = {TEAM_GUN}, -- OPTIONAL, which teams are allowed to buy this shipment/separate gun
    label = "МП5", -- Optional: the text on the button in the F4 menu, if you want it different than the name above
    buttonColor = Color(224, 227, 34, 255), -- Optional: The color of the button in the F4 menu
}) 

DarkRP.createShipment("РПК Пулемет", {
    model = "models/weapons/w_rif_ak47.mdl", -- The model of the item that hovers above the shipment
    entity = "fas2_rpk", -- the entity that comes out of the shipment
    price = 80000, -- the price of one shipment
    amount = 10, -- how many of the item go in one purchased shipment
    separate = true, -- whether the item is sold separately (usually used for guns)
    pricesep = 10000, -- the price of a separately sold item
    allowed = {TEAM_GUN}, -- OPTIONAL, which teams are allowed to buy this shipment/separate gun
    label = "РПК Пулемет", -- Optional: the text on the button in the F4 menu, if you want it different than the name above
    buttonColor = Color(224, 227, 34, 255), -- Optional: The color of the button in the F4 menu
}) 

