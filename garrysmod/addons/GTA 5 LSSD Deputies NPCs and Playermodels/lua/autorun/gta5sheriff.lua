player_manager.AddValidModel( "LSSD Deputy", "models/gta5/lssd/sheriffpm.mdl" )

local lssd_models = {"models/gta5/lssd/sheriffg.mdl","models/gta5/lssd/sheriffb.mdl"}	
hook.Add("PlayerSpawnedNPC","RandomBodyGrouplssd",function(ply,npc)
if table.HasValue( lssd_models, npc:GetModel() )	
then npc:SetBodygroup( 0, math.random(0,8) );
npc:SetBodygroup( 1, math.random(0,1) );
npc:SetBodygroup( 2,2 );
npc:SetBodygroup( 4, math.random(0,2) );

if math.random(0,2) == 2 then
npc:SetBodygroup( 2, math.random(0,1) )
end
if math.random(0,2) == 2 then
npc:SetBodygroup( 3, math.random(1,2) )
end
end
end)

local lssdar_models = {"models/gta5/lssd/sheriffarg.mdl","models/gta5/lssd/sheriffarb.mdl"}	
hook.Add("PlayerSpawnedNPC","RandomBodyGrouplssdar",function(ply,npc)
if table.HasValue( lssdar_models, npc:GetModel() )	
then npc:SetBodygroup( 0, math.random(0,8) );
npc:SetBodygroup( 1, math.random(0,1) );
npc:SetBodygroup( 2,2 );
npc:SetBodygroup( 4,3 );
npc:SetBodygroup( 5,1 );

if math.random(0,2) == 2 then
npc:SetBodygroup( 2, math.random(0,1) )
end
if math.random(0,2) == 2 then
npc:SetBodygroup( 3, math.random(1,2) )
end
end
end)

--ok so this one didnt freak out, weird, maybe it has to do with the order i place the codes, but, this one only used 1 model, so who knows

local nextName
local tbNPCs = {}

local function AddNPC(category, name, class, model, keyvalues, weapons, spawnflags)
		list.Set("NPC",name,{Name = name,Class = class,Model = model,Category = category,KeyValues = keyvalues,Weapons = weapons, SpawnFlags = spawnflags})
		tbNPCs[name] = model
end

AddNPC("Sheriff", "LSSD Deputy (Bad)", "npc_combine_s", "models/gta5/lssd/sheriffb.mdl", {citizentype = CT_UNIQUE, SquadName = "us"}, {"weapon_pistol","weapon_pistol","weapon_shotgun"})
AddNPC("Sheriff", "LSSD Deputy Armor (Bad)", "npc_combine_s", "models/gta5/lssd/sheriffarb.mdl", {citizentype = CT_UNIQUE, SquadName = "us"}, {"weapon_pistol","weapon_pistol","weapon_shotgun","weapon_smg1"})

AddNPC("Sheriff", "LSSD Deputy (Good)", "npc_citizen", "models/gta5/lssd/sheriffg.mdl", {citizentype = CT_UNIQUE, SquadName = "rebels"}, {"weapon_pistol"})
AddNPC("Sheriff", "LSSD Deputy Armor (Good)", "npc_citizen", "models/gta5/lssd/sheriffarg.mdl", {citizentype = CT_UNIQUE, SquadName = "rebels"}, {"weapon_pistol","weapon_smg1","weapon_smg1"})