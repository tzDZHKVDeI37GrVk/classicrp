if SERVER then
	AddCSLuaFile("shared.lua")
end

if CLIENT then
	SWEP.PrintName = "Handcuffs"
	SWEP.Slot = 1
	SWEP.SlotPos = 3
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	SWEP.SwayScale = 0
	SWEP.BobScale = 0
	
	SWEP.DisplayAlpha = 0
end

SWEP.Base = "weapon_cs_base2"

SWEP.Author = "Rick Darkaliono, philxyz"
SWEP.Instructions = "Left or right click to arrest"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.IconLetter = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "stunstick"

SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.Category = "DarkRP (Utility)"

SWEP.NextStrike = 0

SWEP.ViewModel = Model("models/weapons/spy/handcuffs.mdl")
SWEP.WorldModel = Model("models/weapons/spy/w_handcuffs.mdl")

SWEP.Sound = Sound("weapons/stunstick/stunstick_swing1.wav")

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ArrestSoundTime = 0
SWEP.ArrestTargetTime = 0

SWEP.ArrestTime = 3
SWEP.CanArrestFromAnyAngle = false
SWEP.RightClickSwitchToUnarrestBaton = true

SWEP.Handcuffs = true -- identifier, instead of calling GetClass (cheaper this way)
SWEP.UseHands = true
SWEP.ArrestCooldown = 0 -- change this value to add a cooldown for arresting after a successful arrest
SWEP.ArrestWait = 0 -- don't change this

function SWEP:Initialize()
	self:SetHoldType("normal")
end

local scale = Vector(0.75, 0.75, 0.75)
local bones = {"ValveBiped.Bip01_R_Clavicle",
	"ValveBiped.Bip01_R_UpperArm",
	"ValveBiped.Bip01_R_Forearm",
	"ValveBiped.Bip01_R_Wrist",
	"ValveBiped.Bip01_R_Ulna",
	"ValveBiped.Bip01_R_Hand",
	"ValveBiped.Bip01_R_Finger0",
	"ValveBiped.Bip01_R_Finger01",
	"ValveBiped.Bip01_R_Finger02",
	"ValveBiped.Bip01_R_Finger1",
	"ValveBiped.Bip01_R_Finger11",
	"ValveBiped.Bip01_R_Finger12",
	"ValveBiped.Bip01_R_Finger2",
	"ValveBiped.Bip01_R_Finger21",
	"ValveBiped.Bip01_R_Finger22",
	"ValveBiped.Bip01_R_Finger3",
	"ValveBiped.Bip01_R_Finger31",
	"ValveBiped.Bip01_R_Finger32",
	"ValveBiped.Bip01_R_Finger4",
	"ValveBiped.Bip01_R_Finger41",
	"ValveBiped.Bip01_R_Finger42"}

function SWEP:Deploy()
	self:SendWeaponAnim(ACT_VM_DRAW)
	
	if CLIENT then
		local vm = self.Owner:GetHands()
		
		for k, v in pairs(bones) do
			local bone = vm:LookupBone(v)
			
			if bone then
				vm:ManipulateBoneScale(bone, scale)
			end
		end
	end
	
	return true
end

local CT, aim

local fiddlesnd = {"weapons/357/357_reload1.wav", "weapons/357/357_reload3.wav", "weapons/357/357_reload4.wav"}

function SWEP:Think()
	CT = CurTime()
	
	if SERVER then
		if self.ArrestTarget then
			if IsValid(self.ArrestTarget) then
				if self.ArrestTarget:GetVelocity():Length() <= 100 then
					if self.ArrestTarget:GetPos():Distance(self.Owner:GetPos()) <= 70 then
						aim = self.ArrestTarget:EyeAngles()
						aim.p = 0
						aim = aim:Forward()
						
						if aim:DotProduct((self.Owner:GetShootPos() - self.ArrestTarget:GetShootPos()):GetNormal()) <= -0.7 or self.CanArrestFromAnyAngle then
							if self.ArrestTarget:Alive() then
								if CT > self.ArrestSoundTime then
									self.Owner:EmitSound(table.Random(fiddlesnd), 70, 100)
									self.ArrestSoundTime = CT + 1
								end
								
								if CT > self.ArrestTargetTime then
									self.ArrestTarget:arrest(nil, self.Owner)
									self.Owner:EmitSound("weapons/357/357_spin1.wav", 70, 100)
									self.ArrestTarget:Freeze(false)
									self.ArrestTargetTime = 0
									
									GAMEMODE:Notify(self.ArrestTarget, 0, 20, "You've been arrested by " .. self.Owner:Nick())

									if self.Owner.SteamName then
										DB.Log(self.Owner:Nick().." ("..self.Owner:SteamID()..") arrested "..self.ArrestTarget:Nick(), nil, Color(0, 255, 255))
									end
									
									self:SendWeaponAnim(ACT_VM_DRAW)
									self.ArrestTarget = nil
									self.ArrestWait = CT + self.ArrestCooldown
									SendUserMessage("ArrestCooldown", self.Owner)
								end
							else
								self:ArrestInterrupted()
							end
						else
							self:ArrestInterrupted()
						end
					else
						self:ArrestInterrupted()
					end
				else
					self:ArrestInterrupted()
				end
			else
				SendUserMessage("ArrestFailed", self.Owner)
				self.ArrestTargetTime = 0
				self.ArrestTarget = nil
				self:SendWeaponAnim(ACT_VM_DRAW)
			end
		end
	end
end

function SWEP:ArrestInterrupted()
	SendUserMessage("ArrestInterrupted", self.Owner)
	SendUserMessage("ArrestInterrupted", self.ArrestTarget)
	self.ArrestTarget:Freeze(false)
	self.ArrestTargetTime = 0
	self.ArrestTarget = nil
	self:SendWeaponAnim(ACT_VM_DRAW)
end

function SWEP:ArrestInterruptedHolster()
	SendUserMessage("ArrestInterrupted", self.Owner)
	SendUserMessage("ArrestInterrupted", self.ArrestTarget)
	self.ArrestTarget:Freeze(false)
	self.ArrestTarget = nil
end

local vec0 = Vector(1, 1, 1)

function SWEP:Holster()
	self.ArrestTargetTime = 0
	self.DisplayAlpha = 0
	
	if CLIENT then
		if self.Owner == LocalPlayer() then
			local vm = self.Owner:GetHands()
			
			for k, v in pairs(bones) do
				local b = vm:LookupBone(v)

				if b then
					vm:ManipulateBoneScale(b, vec0)
				end
			end
		end
	end
	
	
	if CLIENT or not IsValid(self:GetOwner()) then return end
	SendUserMessage("StunStickColour", self:GetOwner(), 255, 255, 255, "")
	return true
end

function SWEP:OnRemove()
	if SERVER then
		if IsValid(self.ArrestTarget) then
			SendUserMessage("ArrestFailed", self.Owner)
			self.ArrestTargetTime = 0
			self.ArrestTarget:Freeze(false)
			self.ArrestTarget = nil
			self:SendWeaponAnim(ACT_VM_DRAW)
		end
	end
	
	if CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
		self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
		self.Owner:GetViewModel():SetMaterial("")
		
		if self.Owner == LocalPlayer() then
			local vm = self.Owner:GetHands()
			
			for k, v in pairs(bones) do
				local b = vm:LookupBone(v)

				if b then
					vm:ManipulateBoneScale(b, vec0)
				end
			end
		end
	end
	
	if SERVER and IsValid(self:GetOwner()) then
		SendUserMessage("StunStickColour", self:GetOwner(), 255, 255, 255, "")
	end
end

usermessage.Hook("StunStickColour", function(um)
	local viewmodel = LocalPlayer():GetViewModel()
	local r,g,b,a = um:ReadLong(), um:ReadLong(), um:ReadLong(), 255
	viewmodel:SetColor(Color(r,g,b,a))
	viewmodel:SetMaterial(um:ReadString())
end)

function SWEP:PrimaryAttack()
	local CT = CurTime()
	
	if CT < self.NextStrike then return end

	if CLIENT then return end
	
	if CT < self.ArrestWait then
		return
	end

	local trace = self.Owner:GetEyeTrace()

	if IsValid(trace.Entity) and trace.Entity:IsPlayer() and trace.Entity:IsCP() and not GAMEMODE.Config.cpcanarrestcp then
		GAMEMODE:Notify(self.Owner, 1, 5, "You can not arrest other CPs!")
		return
	end

	if trace.Entity:GetClass() == "prop_ragdoll" then
		for k,v in pairs(player.GetAll()) do
			if trace.Entity.OwnerINT and trace.Entity.OwnerINT == v:EntIndex() and GAMEMODE.KnockoutToggle then
				GAMEMODE:KnockoutToggle(v, true)
				return
			end
		end
	end

	if not IsValid(trace.Entity) or (self.Owner:EyePos():Distance(trace.Entity:GetPos()) > 115) or (not trace.Entity:IsPlayer() and not trace.Entity:IsNPC()) then
		return
	end

	if not GAMEMODE.Config.npcarrest and trace.Entity:IsNPC() then
		return
	end

	if GAMEMODE.Config.needwantedforarrest and not trace.Entity:IsNPC() and not trace.Entity:getDarkRPVar("wanted") then
		GAMEMODE:Notify(self.Owner, 1, 5, "The player must be wanted in order to be able to arrest them.")
		return
	end

	if FAdmin and trace.Entity:IsPlayer() and trace.Entity:FAdmin_GetGlobal("fadmin_jailed") then
		GAMEMODE:Notify(self.Owner, 1, 5, "You cannot arrest a player who has been jailed by an admin.")
		return
	end

	local jpc = DB.CountJailPos()

	if not jpc or jpc == 0 then
		GAMEMODE:Notify(self.Owner, 1, 4, "You cannot arrest people since there are no jail positions set!")
	else
		-- Send NPCs to Jail
		if trace.Entity:IsNPC() then
			trace.Entity:SetPos(DB.RetrieveJailPos())
		else
			if not trace.Entity.Babygod then
				if trace.Entity:GetVelocity():Length() <= 100 then
					aim = trace.Entity:EyeAngles()
					aim.p = 0
					aim = aim:Forward()
					
					local DotProduct = aim:DotProduct((self.Owner:GetShootPos() - trace.Entity:GetShootPos()):GetNormal())

					if DotProduct <= -0.7 or self.CanArrestFromAnyAngle then
						self:SetHoldType("revolver")
						self.Owner:SetAnimation(PLAYER_RELOAD)
						self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
						
						timer.Simple(2, function() if self:IsValid() then self:SetHoldType("normal") end end)
	
						self.ArrestTargetTime = CT + self.ArrestTime
						self.ArrestTarget = trace.Entity
						self.ArrestTarget.BeingArrested = true
						self.ArrestSoundTime = CT + 0.6
						self.ArrestTarget:Freeze(true)
						
						if SERVER then
							umsg.Start("HandCuffs_Start", self.Owner)
								umsg.Short(self.ArrestTime)
								umsg.Entity(trace.Entity)
							umsg.End()
							
							umsg.Start("BeingArrested", trace.Entity)
								umsg.Short(self.ArrestTime)
								umsg.Entity(self.Owner)
							umsg.End()
						end
					end
				end
			else
				GAMEMODE:Notify(self.Owner, 1, 4, "You can't arrest players who are spawning.")
			end
		end
	end
end

function SWEP:SecondaryAttack()
	if CLIENT then return end
	
	if not self.RightClickSwitchToUnarrestBaton then
		return
	end
	
	if self.Owner:HasWeapon("unarrest_stick") then
		self.Owner:SelectWeapon("unarrest_stick")
	end
end

if CLIENT then
	surface.CreateFont("HC_16", {font = "Default", size = 16, weight = 700, blursize = 0, antialias = true, shadow = false})
	
	local function HandCuffs_Start(um)
		local time = um:ReadShort()
		local ent = um:ReadEntity()
		
		local ply = LocalPlayer()
		local wep = ply:GetActiveWeapon()
		
		if IsValid(wep) and wep.Handcuffs then
			wep.ArrestTargetTime = CurTime() + wep.ArrestTime
			wep.ArrestTarget = ent
		end
	end
	
	usermessage.Hook("HandCuffs_Start", HandCuffs_Start)
	
	local function BeingArrested(um)
		local time = um:ReadShort()
		local ent = um:ReadEntity()
		
		notification.AddLegacy("You are being arrested by " .. ent:Nick() .. ".", NOTIFY_HINT, time)
	end
	
	usermessage.Hook("BeingArrested", BeingArrested)
	
	local function ArrestInterrupted()
		notification.AddLegacy("Arrest process interrupted!", NOTIFY_HINT, 5)
		
		local ply = LocalPlayer()
		local wep = ply:GetActiveWeapon()
		
		if IsValid(wep) and wep.Handcuffs then
			wep.ArrestTargetTime = 0
			wep.ArrestTarget = nil
			wep.DisplayAlpha = 0
		end
	end
	
	usermessage.Hook("ArrestInterrupted", ArrestInterrupted)
	
	local function ArrestFailed()
		notification.AddLegacy("Arrest process failed!", NOTIFY_HINT, 5)
		
		local ply = LocalPlayer()
		local wep = ply:GetActiveWeapon()
		
		if IsValid(wep) and wep.Handcuffs then
			wep.ArrestTargetTime = 0
			wep.ArrestTarget = nil
			wep.DisplayAlpha = 0
		end
	end
	
	usermessage.Hook("ArrestFailed", ArrestFailed)
	
	local function ArrestCooldown()
		local ply = LocalPlayer()
		local wep = ply:GetActiveWeapon()
		
		if IsValid(wep) and wep.Handcuffs then
			wep.ArrestWait = CurTime() + wep.ArrestCooldown
		end
	end
	
	usermessage.Hook("ArrestCooldown", ArrestCooldown)
	
	local x, y, amt, a, nick
	local White, Black, Grey, Green = Color(255, 255, 255, 255), Color(0, 0, 0, 255), Color(50, 50, 50, 255), Color(168, 216, 125, 255)
	local LightGrey = Color(213, 213, 213, 255)
	
	function SWEP:DrawHUD()
		CT = CurTime()
		
		if CT < self.ArrestWait then
			x, y = ScrW(), ScrH()
			Black.a, White.a = 255, 255
			draw.SimpleText("Next arrest available in " .. math.ceil(self.ArrestWait - CT) .. " second(s)", "HC_16", x * 0.5 + 1, y * 0.5 + 106, Black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			draw.SimpleText("Next arrest available in " .. math.ceil(self.ArrestWait - CT) .. " second(s)", "HC_16", x * 0.5, y * 0.5 + 105, White, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end
		
		if CT < self.ArrestTargetTime then
			self.DisplayAlpha = math.Approach(self.DisplayAlpha, 1, FrameTime() * 10)
		else
			self.DisplayAlpha = math.Approach(self.DisplayAlpha, 0, FrameTime() * 10)
		end
		
		if self.DisplayAlpha > 0 then
			x, y = ScrW(), ScrH()
			a = self.DisplayAlpha * 255
			
			White.a = a
			Black.a = a
			Grey.a = a
			Green.a = a
			LightGrey.a = a
			
			if IsValid(self.ArrestTarget) then
				nick = self.ArrestTarget:Nick()
				draw.SimpleText("Arresting " .. nick .. "...", "HC_16", x * 0.5 + 1, y * 0.5 + 106, Black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				draw.SimpleText("Arresting " .. nick .. "...", "HC_16", x * 0.5, y * 0.5 + 105, White, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			end
			
			amt = (self.ArrestTime - (self.ArrestTargetTime - CurTime())) / self.ArrestTime
			amt = math.Clamp(amt * 200, 0, 200)

			if amt > 2 then
				draw.RoundedBox(4, x * 0.5 - 101, y * 0.5 + 119, 202, 18, Black)
				draw.RoundedBox(4, x * 0.5 - 100, y * 0.5 + 120, 200, 16, Grey)
				draw.RoundedBox(4, x * 0.5 - 100, y * 0.5 + 120, amt, 16, Green)
				
				draw.SimpleText(math.Round(amt * 0.5) .. "%", "HC_16", x * 0.5 + 1, y * 0.5 + 128, Black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				draw.SimpleText(math.Round(amt * 0.5) .. "%", "HC_16", x * 0.5, y * 0.5 + 127, White, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			end
		end
	end
	
	SWEP.BlendPos = Vector(0, 0, 0)
	SWEP.BlendAng = Vector(0, 0, 0)
	SWEP.OldDelta = Angle(0, 0, 0)
	SWEP.AngleDelta = Angle(0, 0, 0)
	
	local reg = debug.getregistry()
	local Right = reg.Angle.Right
	local Up = reg.Angle.Up
	local Forward = reg.Angle.Forward
	local RotateAroundAxis = reg.Angle.RotateAroundAxis
	local GetVelocity = reg.Entity.GetVelocity
	local Length = reg.Vector.Length
	
	local Vec0 = Vector(0, 0, 0)
	local TargetPos, TargetAng, cos1, sin1, tan, ws, rs, mod, vel, FT, sin2, delta
	
	local SP = game.SinglePlayer() 
	local PosMod, AngMod = Vector(0, 0, 0), Vector(0, 0, 0)
	local CurPosMod, CurAngMod = Vector(0, 0, 0), Vector(0, 0, 0)

	function SWEP:PreDrawViewModel()
		CT = UnPredictedCurTime()
		vm = self.Owner:GetViewModel()
		
		EA = EyeAngles()
		FT = FrameTime()
		
		delta = Angle(EA.p, EA.y, 0) - self.OldDelta
		delta.p = math.Clamp(delta.p, -10, 10)
			
		self.OldDelta = Angle(EA.p, EA.y, 0)
		self.AngleDelta = LerpAngle(math.Clamp(FT * 10, 0, 1), self.AngleDelta, delta)
		self.AngleDelta.y = math.Clamp(self.AngleDelta.y, -10, 10)

		vel = Length(GetVelocity(self.Owner))
		ws = self.Owner:GetWalkSpeed()
		
		PosMod, AngMod = Vec0 * 1, Vec0 * 1
		
		if vel < 10 or not self.Owner:OnGround() then
			cos1, sin1 = math.cos(CT), math.sin(CT)
			tan = math.atan(cos1 * sin1, cos1 * sin1)
			
			AngMod.x = AngMod.x + tan * 1.15
			AngMod.y = AngMod.y + cos1 * 0.4
			AngMod.z = AngMod.z + tan
			
			PosMod.y = PosMod.y + tan * 0.2
		elseif vel > 10 and vel < ws * 1.2 then
			mod = 6 + ws / 130
			mul = math.Clamp(vel / ws, 0, 1)
			sin1 = math.sin(CT * mod) * mul
			cos1 = math.cos(CT * mod) * mul
			tan1 = math.tan(sin1 * cos1) * mul
			
			AngMod.x = AngMod.x + tan1
			AngMod.y = AngMod.y - cos1
			AngMod.z = AngMod.z + cos1
			PosMod.x = PosMod.x - sin1 * 0.4
			PosMod.y = PosMod.y + tan1 * 1
			PosMod.z = PosMod.z + tan1 * 0.5
		elseif (vel > ws * 1.2 and self.Owner:KeyDown(IN_SPEED)) or vel > ws * 3 then
			rs = self.Owner:GetRunSpeed()
			mod = 7 + math.Clamp(rs / 100, 0, 6)
			mul = math.Clamp(vel / rs, 0, 1)
			sin1 = math.sin(CT * mod) * mul
			cos1 = math.cos(CT * mod) * mul
			tan1 = math.tan(sin1 * cos1) * mul
		
			AngMod.x = AngMod.x + tan1 * 0.2
			AngMod.y = AngMod.y - cos1 * 1.5
			AngMod.z = AngMod.z + cos1 * 3
			PosMod.x = PosMod.x - sin1 * 1.2
			PosMod.y = PosMod.y + tan1 * 1.5
			PosMod.z = PosMod.z + tan1
		end
		
		FT = FrameTime()
		
		CurPosMod = LerpVector(FT * 10, CurPosMod, PosMod)
		CurAngMod = LerpVector(FT * 10, CurAngMod, AngMod)
	end

	function SWEP:GetViewModelPosition(pos, ang)
		RotateAroundAxis(ang, Right(ang), CurAngMod.x + self.AngleDelta.p)
		RotateAroundAxis(ang, Up(ang), CurAngMod.y + self.AngleDelta.y * 0.3)
		RotateAroundAxis(ang, Forward(ang), CurAngMod.z + self.AngleDelta.y * 0.3)

		pos = pos + (CurPosMod.x + self.AngleDelta.y * 0.1) * Right(ang)
		pos = pos + (CurPosMod.y + self.BlendPos.y) * Forward(ang)
		pos = pos + (CurPosMod.z + self.BlendPos.z - self.AngleDelta.p * 0.1) * Up(ang)
		
		return pos, ang
	end
end

local function HC_Move(ply, d)
	local wep = ply:GetActiveWeapon()

	if IsValid(wep) then
		if wep.Handcuffs then
			if CurTime() < wep.ArrestTime then
				d:SetMaxSpeed(0)
			end
		end
	end
end

hook.Add("Move", "HC_Move", HC_Move)

local function HC_CanPlayerSuicide(ply)
	if ply.BeingArrested then
		return false
	end
	
	return true
end

hook.Add("CanPlayerSuicide", "HC_CanPlayerSuicide", HC_CanPlayerSuicide)

local function HC_playerArrested(crim, time, cop)
	crim.BeingArrested = false
end

hook.Add("playerArrested", "HC_playerArrested", HC_playerArrested)