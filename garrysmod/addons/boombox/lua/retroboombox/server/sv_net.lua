util.AddNetworkString( "RetroBoombox:ChangeSound" )
util.AddNetworkString( "RetroBoombox:TurnPower" )
util.AddNetworkString( "RetroBoombox:TurnPlaying" )
util.AddNetworkString( "RetroBoombox:ChangeFrequence" )
util.AddNetworkString( "RetroBoombox:ChangeLightMode" )
util.AddNetworkString( "RetroBoombox:GrabBoombox" )
util.AddNetworkString( "RetroBoombox:SendBoomboxInfos" )

local function checkBoomboxNet( eBoombox, pPlayer )
	if not IsValid( eBoombox ) or not eBoombox.IsBoombox then return false end
	if eBoombox:GetPos():DistToSqr( pPlayer:GetPos() ) > 62500 then return false end

	if RetroBoombox.Config.BoomboxUseLimitedToOwner and eBoombox.GetBoomboxOwner and IsValid( eBoombox:GetBoomboxOwner() ) and eBoombox:GetBoomboxOwner() ~= pPlayer then
		return false
	end

	return true
end

net.Receive( "RetroBoombox:GrabBoombox", function( len, pPlayer )
	local eBoombox = net.ReadEntity()
	local shouldContinue = checkBoomboxNet( eBoombox, pPlayer )

	if not shouldContinue then return end

	if pPlayer:HasWeapon( "retroboombox_base" ) then
		DarkRP.notify( pPlayer, NOTIFY_ERROR, 10, RetroBoombox:L("AlreadyCarryBoombox") )
		return
	end

	local boomboxSwep = pPlayer:Give( "retroboombox_base" )
	if not IsValid( boomboxSwep ) then return end

	pPlayer:SelectWeapon( "retroboombox_base" )

	timer.Simple( 0.1, function()
		if not IsValid( boomboxSwep ) then return end
		if not IsValid( eBoombox ) then return end

		hook.Run( "onBoomboxGrabbed", pPlayer, boomboxSwep, eBoombox )
		
   		if eBoombox.GetBoomboxOwner and IsValid( eBoombox:GetBoomboxOwner() ) then
   			boomboxSwep.BoomboxOwner = eBoombox:GetBoomboxOwner()
   		end

		boomboxSwep:SetPower( eBoombox:GetPower() )
		boomboxSwep:SetPlaying( eBoombox:GetPlaying() )
		boomboxSwep:SetFrequence( eBoombox:GetFrequence() )
		boomboxSwep:SetSoundLevel( eBoombox:GetSoundLevel() )
		boomboxSwep:SetLightMode( eBoombox:GetLightMode() )
		boomboxSwep.BoomboxClass = eBoombox:GetClass()

		eBoombox:Remove()
	end )

	local tColors = {}
	tColors.MainColor = eBoombox.MainColor
	tColors.SecondaryColor = eBoombox.SecondaryColor
	tColors.TubeLightsColor = eBoombox.TubeLightsColor
	tColors.MainLightsColor = eBoombox.MainLightsColor
	tColors.SoundLightsColor =  eBoombox.SoundLightsColor 
	tColors.ScreenBackgroundColor = eBoombox.ScreenBackgroundColor
	tColors.ScreenContentColor = eBoombox.ScreenContentColor

	boomboxSwep:GiveProperties( tColors )

	net.Start( "RetroBoombox:SendBoomboxInfos" )
		net.WriteTable( tColors )
	net.Send( pPlayer )

end )

net.Receive( "RetroBoombox:ChangeLightMode", function( len, pPlayer )
	local eBoombox = net.ReadEntity()
	local shouldContinue = checkBoomboxNet( eBoombox, pPlayer )

	if not shouldContinue then return end

	RetroBoombox:ChangeLightMode( eBoombox, eBoombox:GetLightMode() + 1 )
end )

net.Receive( "RetroBoombox:ChangeFrequence", function( len, pPlayer )
	local eBoombox = net.ReadEntity()
	local shouldContinue = checkBoomboxNet( eBoombox, pPlayer )

	if not shouldContinue then return end

	local iFrequence = net.ReadInt( 3 )

	if RetroBoombox.Config.Frequencies[ eBoombox:GetFrequence() + iFrequence ] then
			eBoombox:SetFrequence( eBoombox:GetFrequence() + iFrequence )
	elseif iFrequence == -1 then
		eBoombox:SetFrequence( #RetroBoombox.Config.Frequencies )
	elseif iFrequence == 1 then
		eBoombox:SetFrequence( 1 )
	end

end )

net.Receive( "RetroBoombox:ChangeSound", function( len, pPlayer )
	local eBoombox = net.ReadEntity()
	local shouldContinue = checkBoomboxNet( eBoombox, pPlayer )

	if not shouldContinue then return end

	local iSound = net.ReadInt( 3 )
	eBoombox:SetSoundLevel( math.Clamp( eBoombox:GetSoundLevel() + iSound * ( RetroBoombox.Config.MaxSoundVolume / 20 ), 0, RetroBoombox.Config.MaxSoundVolume ) )
end )

net.Receive( "RetroBoombox:TurnPower", function( len, pPlayer )
	local eBoombox = net.ReadEntity()
	local shouldContinue = checkBoomboxNet( eBoombox, pPlayer )

	if not shouldContinue then return end

	local bPower = net.ReadBool()
	eBoombox:SetPower( bPower )
end )

net.Receive( "RetroBoombox:TurnPlaying", function( len, pPlayer )
	local eBoombox = net.ReadEntity()
	local shouldContinue = checkBoomboxNet( eBoombox, pPlayer )

	if not shouldContinue then return end

	eBoombox:SetPlaying( not eBoombox:GetPlaying() )
end )
