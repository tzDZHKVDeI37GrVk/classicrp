hook.Add( "PlayerButtonDown", "RetroBoombox:PlayerButtonDown", function( pPlayer, iKey )
	pPlayer.keyCurrentlyPressed = pPlayer.keyCurrentlyPressed or {}
	if IsValid( pPlayer:GetActiveWeapon() ) and pPlayer:GetActiveWeapon():GetClass() == "retroboombox_base" then
		pPlayer.keyCurrentlyPressed[ iKey ] = true

		if pPlayer.keyCurrentlyPressed[ RetroBoombox.Config.KeyBase ] and pPlayer.keyCurrentlyPressed[ KEY_E ] then
			pPlayer:GetActiveWeapon():SetPower( not pPlayer:GetActiveWeapon():GetPower() )
		end
		if pPlayer.keyCurrentlyPressed[ RetroBoombox.Config.KeyBase ] and pPlayer.keyCurrentlyPressed[ KEY_UP ] then
			pPlayer:GetActiveWeapon():SetSoundLevel( math.Clamp( pPlayer:GetActiveWeapon():GetSoundLevel() + ( RetroBoombox.Config.MaxSoundVolume / 20 ), 0, RetroBoombox.Config.MaxSoundVolume ) )
		end
		if pPlayer.keyCurrentlyPressed[ RetroBoombox.Config.KeyBase ] and pPlayer.keyCurrentlyPressed[ KEY_DOWN ] then
			pPlayer:GetActiveWeapon():SetSoundLevel( math.Clamp( pPlayer:GetActiveWeapon():GetSoundLevel() - ( RetroBoombox.Config.MaxSoundVolume / 20 ), 0, RetroBoombox.Config.MaxSoundVolume ) )
		end
	end
end )

hook.Add( "PlayerButtonUp", "RetroBoombox:PlayerButtonUp", function( pPlayer, iKey ) 
	pPlayer.keyCurrentlyPressed[ iKey ] = nil
end )

-- Prevent weapons to be dropped :
hook.Add("canDropWeapon", "RetroBoombox:canDropWeapon", function( pPlayer, eWeapon )
	if not IsValid( eWeapon ) then return end
	if eWeapon:GetClass() == "retroboombox_base" then return false end
end)

-- Make that if you spawn it with DarkRP F4 menu, the limit works.
hook.Add("onBoomboxGrabbed", "RetroBoombox:onBoomboxGrabbed", function( pPlayer, wBoombox, eBoombox )
	local eOwner = eBoombox.Getowning_ent and eBoombox:Getowning_ent() or Player( eBoombox.SID or -1 )
	if eBoombox.DarkRPItem and IsValid( eOwner ) then 
		wBoombox.DarkRPItem = eBoombox.DarkRPItem
		wBoombox.DarkRPOwner = eOwner
	end
end )

hook.Add("onBoomboxDropped", "RetroBoombox:onBoomboxDropped", function( pPlayer, wBoombox, eBoombox )
	if wBoombox.DarkRPItem then
		if IsValid( wBoombox.DarkRPOwner ) then
   			eBoombox.DarkRPItem = wBoombox.DarkRPItem
   			eBoombox.DarkRPOwner = wBoombox.DarkRPOwner
   			eBoombox.SID = wBoombox.DarkRPOwner.SID
			local owningEnt = eBoombox.Setowning_ent and eBoombox:Setowning_ent( wBoombox.DarkRPOwner )
			local owner = wBoombox.DarkRPOwner and wBoombox.DarkRPOwner:addCustomEntity( eBoombox.DarkRPItem )
		else
			eBoombox:Remove()
		end
	end
end )

-- Set ownership once bought in DarkRP
hook.Add("playerBoughtCustomEntity", "RetroBoombox:playerBoughtCustomEntity", function( pPlayer, tEntTable, eBoombox, pPrice )
	if eBoombox.SetBoomboxOwner and isfunction( eBoombox.SetBoomboxOwner ) then
		eBoombox:SetBoomboxOwner( pPlayer )
	end
end)
