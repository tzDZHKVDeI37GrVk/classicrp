RetroBoombox.Config.Language = "ru"

--[[
	https://wiki.facepunch.com/gmod/Enums/KEY
]]
RetroBoombox.Config.KeyBase = KEY_LALT

RetroBoombox.Config.ShouldUseParticles = false
RetroBoombox.Config.MaxSoundVolume = 200
RetroBoombox.Config.BoomboxCanBeDestroyed = true

--[[
	If this parameter is set to false, everyone can use
	every boombox.

	If this parameter is set to true,
	if the boombox is owned by someone, only the owner can use it.
	if the boombox is not owned by someone, everyone can use it.
]]
RetroBoombox.Config.BoomboxUseLimitedToOwner = true

RetroBoombox.Config.Frequencies = {
	-- Russian
	 {
		 name = "МарусяFM",
		 logo = "https://top-radio.ru/assets/image/radio/180/radiomarusya.jpg",
		 url = "http://radio-holding.ru:9000/marusya_default"
	 },
	 {
		 name = "Русское радио",
		 logo = "https://top-radio.ru/assets/image/radio/180/rusradio.png",
		 url = "http://151.80.97.38:8024/2capitalesradio"
	 },
	 {
		 name = "Релакс",
		 logo = "https://guzei.com/online_radio/logo300/1433.jpg",
		 url = "http://klassikr.streamabc.net/klr-smooth-mp3-128-7968529?sABC=5r9p9399%230%23n59pr3orr59n03r05s8539333s0r87oq%23jjj.xynffvxenqvb.qr&amsparams=playerid:www.klassikradio.de;skey:1587319705"
	 },
	 {
		 name = "Хит ФМ",
		 logo = "https://hitfm.ru/uploads/fc/4f/11f8a111395f7d9eb83ee529f552.png",
		 url = "http://hitfm.hostingradio.ru/hitfm128.mp3"
	 },
	 {
		 name = "Радио рекорд",
		 logo = "http://top-radio.ru/assets/image/radio/180/record.png2",
		 url = "http://air.radiorecord.ru:805/rr_320"
	 },
	 {
		 name = "Запрещенное радио Т",
		 logo = "https://i.pinimg.com/originals/77/a2/69/77a26962c42abc24f5c4fe86b440b885.png",
		 url = "https://podrubasik.ru/radio/8020/radio.mp3?1587373893"
	 },
	 {
		 name = "Чеснок",
		 logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA7VBMVEX////933H/22EyAAD/43P/5XT/53X93m393WYvAAAVAAD/2lv/6XYkAAAZAAD/x2L/1mL/32LmxWMeAAAoAAD/z2L21WsrAAAcJjWCYTD/tWT95pf/vGPYuV3MrFf/oWT/wWP//PT/r2P/qWT++eb+8cbBolEQIDRhPx/+9diff0BnSyZwVStXNBn+7ru3mEz/mmWrjEYAAAA8BgAAGDOSczr94Xv96aT95IpLJRKUf0pCGQs9EAY0NzonLjepj0+JdkhfVkBuYUNIRTwAEDJ8bEXstF3MoFeke02NaEjkuluyiVHmoFzcj1q/f1KZRRCJAAAUHUlEQVR4nO2dCXvaSNLHI3SAZISNRIQJg8bE3AY0EM74tpM5dvd9v//H2ao+pJYEBhIEcpb/M884CMn0T91dVV1dyB8+nHTSSSeddNJJJ5100kknnXTSSSftrnq9fuwmJKR6ozmcVxeL2Wy2WFTnw2b3VyKtd+cLKXOGymTwP/IvaTH/NSjr3WqWoMUER7PV5nuHbMyllXQBpTTvHruRP6FuNfMWHoPMLN4r41Z875ixPt+SjzLOG8du8K5qStvzEcZs89hN3kn16m58hLH6jsxqd7YFYJFIRJTezWxs5jYCFouO63mt2kUIMTM8dtO303AzX6b18jABte+/WiHG+bEbv43mmwFrr5P2JVF78uBlBMb3gLgFoNduIxsKfz6FEKvHBtikLQBdwvf86NZaT/fA+O3rexqom+dgxrlvX06ea9SUfvwOiJPW+0HsbuTLFL9OAJDb0GIGXrbvHfGMsxT7/kZ2cxdeQBc+BAa0mHkB4sdi6JzU+sW66OjD3jw47AKQOPGK1sNl+/Ui1ImztEY3gZUBOseyLjJxSByk7Zp4vAhTsf2vC5SPmFKD2uWAxQv3++vD5eX986MTZSy+tC/vxR67yLiTb6/nH0ECYiqnoj9Gi63nCfXo7cm9Fx2sz+32swgIZH/9/efH8/NzkVFK4zj1x+jXCXPo+OPb98hQfQ0RAiCinV9dXTFG1okpdBmNHGv00zcAa78+ff0KsSc4u5dMjJAzXxDAq6vfiK5ExEz6VsSsC4seUj3XiC21iD8PhSw4Sl9DPQh8n4gYI0VMn7FhXVhEdwdMFKpYfMTIUzSdxe/fvj2HAT99+kwEjAJi6pwi70JAmnwPiIhzEC1L0Xp55sRICICfP18TUcS0dmIj6w/Cy0thyVfEA2H/l8nEAX8HIeJvAmIuXTORRdxFBwKUl5A/b02iMzGYhWSMfiZ8hPFzqBPTZU4XjLDWjgSZMDEn3x7XETLAP1CsF2EmMsJUxW58TUF6zA3xFN3np4vMCvFBygn/oJ3oE2bO0mRruLdHQxObdWti8IDwD5Hw6jyVXn/xBuE6rSf8mEndMG34/eXhKM1cXISWCpsIf1/dh2lyiU1/VeF+u/z2F0bT4bXCGsQI4XV4HmbO0pM+DRaGH5/bz+e4UDg/38hIvAW6Q4LoewuBMD1On09DDKX/PMdYmiwVxEB6zTDFkO36mrvDkD/EiXhsMK56NmhzsFa4okHmm4j/fnj9zycWtDFA39BkUhTWNPwWi0uFT8j4NuK/HtqTf35jgfdn/4rghLSYGp6+YNYxWCr8FmlwRCSx+PcVvyPhpQUZpmlJZrCgNAD0B91v528gYpbt8uHPc7YA5stDkTAtPn/OCVkkfb3acEQBSab074/MMkXSGGkk9D349e+CbVzfiWTpeP+RGKdz7l/CszY17qJ6FurCYK3wRieSdP5lu1Xk4cGqGCGFhGKI8sdbhMXME991uri4WBcCnS2OjcbkE15FCa//s5KwWLSeMUf1xA+sCWPTT/h/9+1/ruKExYvHhxDgOqWGcL6O8B/cSPszAljMtF7J3u/jim2NCGFa5mFgS5kp5Zbm/5Gk/eJe0FUw3RP1njEZPnl1Ny8jU0M4DHuL39la4fr609+4Yz9pvz55Ncv5aFmtx5d7wtd+iu3ZrCJMiz9sBh4/SA7S1d5fr2wTo335cP/Qpjs2k8lLbXVmI0qYlgViNxK1XQcZ3vPzf79OeGkJqy+5fHE3zkBGmJa4lK0tyNopiLxZCvvio/v0ejlhat8/f7W25MukZ21Rl3xEug8R2mnBDWHLffz69PT1sVVzMluNT6psalJRC2H55G+W8SUwvlH0tTUdKjVr/CBPQxDZUuFqUxZjo1JjSoUdfJbHOA9SUT8BmB5DIyRq6LbuuqXCrkpNmuaDkGzLBGuFjSnhTUpNVIqKlrP9NB0hTM80FMoU9qoUDVK+gNqrUjVIRWu6P8L0WFJUfbZ3wrSVRW1ROrubUmVnUPUdvyOzUdlU2RnUnjsxdV0YKZ/9eaVtFqL2ak5TZkiZ9ugTU+YLubapY99WqTMzVM19EaYmAxXT5i/MbAeYljRpXPXFPhDTVCkUU2Mffj99vl5Udw/LqLSkENfo5xFT6QlFbfHlrjd0ln5A7MUfn4tnaR+iVD9ubs6y7wIQEH/QaZwtUm1FRdV/xPWfnc1T7Adj6u48Ut/bYyN2e/AHdmD13YxQX93Fm8/eCQPO3omJiag524rx7Gw2fE8zMKTmYuNYPTtbvO9nRXXffFAUPgvrfY5PUfVulTzKLE6Xkd7/o76Y6t3hfJGlz2mjymQX1eGv8bi2QPVGtzkczufz4bDZbfxicCeddNJJJ5100kn/42rQRyrDj2O3JAk1htVsDjSjP36VJyv7mmdz2ayE8n8A5a/DWJ/lpBXKvpec8WaxfoureMycXL2xt0fKV9cBSllpTx+xTjD7Q2o2/OMSmgNJPBLSsIEPZ68uZjNJmvEFfqM5F34JOdLFI2sBJSmX5CZ/fZGD2R8SQFU/dMlxdo/hn9XuijOJPcSfZDrBq9l8nmVnwS+hvT/M+ues7cQEK1G6uZWfjI3adGTldSIIMSFwX6InKSr7qQSdmBjgcKV125uK3XrsviiOO7XIP0aexQ9mkyp5ayQLCFrEAGvjQqGflyTNGtvjEetOKZeQx5C2GHg/p/gHOGNZtnuOU7vVZcPvRCmXiN/vJt6FK6RVbFk2DaOgy2YlHxAmshE33KILBXOwL3WMki7LctkYCAeTmYn+JNHyglT/U1VNsmo1S8kHlKEzNXILVPynBP9X+Wkqe0u4T+wInqtobl8fj+2Op+BxfkoiX/nigM5AVN835t5gbBQKhnHT0laeOXDgLLcykI2xMTYH05ZEGNUK/JJQ1ysjerLqkZ+KBncjr6mS2loO+FRMYiLW+TS0xrqgAm2cat0VcDDR8eSoK84cW4hj6/Q0vVTQPbwVqlzWC6Eu1G5MdrKpG7UA3jHLhsteJvGNqIDQgPZRQUtN2hE1A17opm2b8LNsWgo/U44QlvBt08S5pRtTGHXqFzAiYcJ+mdhNPLkQEOY78PIQhAq0W7/tEPmEFnag0fFct3dnQttl/17cdZgGHYc2ulzp9aYVmVwBHm5bQm1UkA9FWJBNjxoPQCGEWqcMn97KK4qiqlNoCbXsQFjuhywNabSlqaqWd+Fi2Xa2JVTIgEiUkEc0iguEI/JBnFCpwacbLjMw+Qr0Ig5JQngTspNCo1ULTrM9Zds+vNUPR2hDw1SRUIOmlAR/XIbDPXUDoZSfmnJpqm5HqMHJSRPykEb1gLAV7kP4dMHoqT1T1pf5TYRKy4ZBrG1FqLho3g5EqIxM/rmcEG1PSSDBKVNwNhLCYIC3KaGqBuHQCkLHxKitnCwhD9pUGC+GJRKSzhBJVDiOTSOWRnIsx1E1JUaojvxRWnZdr+VaqraGMH9TgvgbhkaihHNOCJ9rOJJAiE01pwKhdqOTgUzMH4TNhi33W6SPQvNwoHNLI9uggiH3HGUVITqK8gDnbaKEPHOiQcMMNUQIH22OggCVNM30GCGNYMqFu5oqEir5HqwaAIQQsrNM21XjhMQD246WNOGME0J4chci1AihEFryAyT6wTgHAx19DI2ljYag23Fv0G1CUEP7ENZHNjkJxn+McAn3tKUlTugH3gVZZ5NuC0J9OfK80ZQHOtho/e7u7kuhADFC6VaiUVvJtayaN4BO1ZdalDA/tUkEkTSh7w5rBbQPb4/SqT9KIaYBM6lq0EoIehQSl7KIVi/gAoLFNNBTBAW9TphQq41pFJgwob/AR3fIO0ywNKWKQIhNZJaG29g8xHVwDo28S6WSaRu3Hl0EBv4QbQ/cqjChpOvUdidLOMyI/eNbw8Bb6IO8QLgMvAUjVOk5NPKe9qc9z+IOJCBk/iNEaPXRUWhJ96GQZSOm1JFEQmJRDElQ3OPj4IY5Rhud1yD29m9ZQKjA+IgQlmDkljsan92JEQZJMMUhLQ0TYtRmt/w2kz6NRG2EkPahsOSLElLHKhKyBUjShEKWDVvve3cx8vax2XSKRN6qZ/J5uJ4wD2gwx0OEMDrYvUuSUMiyYSP8jwmtnuwRm4l5XKpGVk8KLn5gRfImoUpGuxUh9JOISRJWgyybAy7uDuYRESGEF3QFbIzICjg/MsIrYFVV89YAerkUS0z4hCpMzLxb0snoDo/SO35ikoT+IO10wErKtzwxgVkMHX7guh/ny8Bz3RHx234WQ1/2er1pB7M445ayjlCfwkm3+Dsw8AkRsiD/QISYO5PF5BJ9ASNSdddlonTTZEFbL7+2D+USy02N0UeGvEXP90IHITQEuoASW6NZt0I20eLZxLKfOpRbNE8TThDiIb3ETzJuSSZE6/vZRLMTuFmIi/SksokCob4MZ3kHS7ZjsjIjPL1Zlsawerqt8PSvtyTJXkHKqL+0x8a4sJy6NA+u9JYsI7xcCucqreVgmVBGWCA0LG5mmLEhx2gL8iSrL2lBF6l4CkwsLUjhwwEpLEUjx4STFHaSGj5XEa7dc1ZfICyEO4AsNYRdrwR2ZlZq3zszAmEp9mGmSHgo7Xt3jYfd1tiQ85HPysvG+McIf2a/dd8bM3x5r9TcWnQYrjq2XSObP864911unoSCaRaHWXVsm0bO11V2xBUtPsntfX/0B/a3g/Ka1c0kVT+NWRRxVSFNtjgPFaFkE6gYqhfjH/tmTY80634YioUVpDxomA06jTnseeh35nKL4TDyOXAhDslhUFo0S6KubR7txOyiGqsMiZzQ/BA0FmukyK3yx3t20f3QrYZuU3bR6Dar0V/jz7g6KQabJ1WZGBlOpO5qA2JWnGWsZFK4U9mcFCukmsWGNvyaAz0Qqx5CzJKnVL1VXBcT+aPp8ZKuLXSwgtJ5JijUJc/H2bEEDELl2Fjf9tIDdSOruc4uhuSe7loClp11M5vPWoN4uEcONRq+HZvv6q7fNr4bEI/xdLq37cy+lVSp3hvya08UVQ2WOAquleD/QqGUGl0s4RWKcIUiOYoqrLjwF8JKSlhMoQ7/YG9/R9iqTCt8RZW3poMC1jv1HMal9IJ3OUGtMm2xxquSdyPDElnusxUy7gyPKjedzrJzM/Wc4O4cymkEavJd/Z5RGvMGV8YmTd2YRo82zhmbhWl4OaJ8MW2WRNVaJZvkOXB70aWnqWW7VMbsSNm0CxXH78dk6i23IjR53YJ0i1toJdMsY9FEn0A4NqZOxeGm9fwtcW1K01csUzXyN2qAuEwLrkwr6aLZHQjVTgmaJE9HvT7mpAokMY6E+kCcik5BZoQqJlb1Qqc3mi7hNHlMctskudi5uel8KWC21M8pHHyYxggVD7djpgqmdq1lmWbgCKFc8ALLo/VLnBAzd+WlRTLGHt4J099OxASQZlXgZpT6bIwnWaC/JSHmv9mUUxx4UapojFAv+dOJ5P8pIbLqd+ydPNbLmLjxGiT52U4/78TkCvS3I8SSHr3kZ9SwT2zeh7JZ4eMUdxYZIVZwB6lTLLQgeyBiAZF6pye7c78LIdnYngYbbDptGyX0SbBsRKeEeBOEbVXFMWjFk0io4S/lBQKHNjVRQr7ty9uG9SYjlVoaMB23lN0xdf2WEbIz/CtuymRLLtSHYgnEsQkdU5gyaHfo1j4QlisAbxMWsvMA7yBhPnxLyO5oGVxMqA9ZPUAKCPNk100W2uvCGOxoSFiq1NimN1b+lDvgLpBQKYduCd0kvs2Hy02xSIHfhUO7fJEQ9zKgR0S/R8qY7hghVqWh1SclABYjxPFriKEAXiGrIVsKXc726VCHBQwRsgIZXdgnIu3V85RQIzSu1jLQn1iMsMArqNk9wVGgc3+ItQxWH2tw+FQ9qsdfQyibjFDFSA1GIHSITYqoNxHqNxDTyBjTmAN+SjLfktmSUB/cdAY4StUQof6FE0rKLYYDMGMh9rTWjFKBUC7TuFQvBN+TOfgfZAlZGjeftyKWhhSXaD4hGpsSdRqMUCLGV7iCzFxWyEfXFgU98CbZg/9dpJi3sENFQ6zCmRNKeQxHZbKDywi1LxFvQaxvnkXeg2Wn33OlYFAcfJDGYxo51GA8jCEOJ6RLCjJROSF6eMHj4xWlKfOHGL6H1vgHj7tXxDRY7NMLioY6Ou9aSqiObFZWwQjpBA5sE6kYbikrvn5BuvDwzxmIRd4jk7gzbjYK1J/7hGBsxrSUihGSb4gEkTd96az6khDoGI/eja2eyJc9/bIo9NUdLURoeTSC4ZYmf4vm1y+WgU4nh1cRZo7xDPoYIVnaGi5++VDJewYzKwGhxFNNnFBpocvs0wJ3tW+ydEecMHuEVOIqQhiYWFpTqTmWW0Hf0CEGxg7X1gqE+CU8QLz1LMfyMMVjk/VzjPBYz/pYkadpjbG4qWAYJMFSJmPyLULJucUTbfy2AlxpdmjFTYgwm5OO9ejdFbk2zbVN/rUC+44m2MDixAiDgkWH1LvRK8Z99gu/6HpAODzeUz6Cb0GN7TFLNKjO1DZs07YNecRnnV4YT8OETsE2OLTWGhg2XFEwOixdKqkDo/CFXXz4QEaQv/WkSL4NwRx2reV5LUsVoVYUMviHlLzleiPPdYKvRyvB24dPdIuKN5u2T1HUFUhvCM5fV81x+FBN1E47wD+o4/6Bx+auG7q77x8m/qidDdqxE7OLnatyjv43OnfqlOzu+/iJPkpoK0XqM+J1IuILUiIUQszGar7C9SnHB/yALc6xRyLlcsOh8FwhUro0ZKVf5BFJ9IJuNrhgIT55iZ7Ej+AL6dhDlKrenS+gebw8o0mfhYUVXt0GOTCfwbvik/LgHHwOFj2El9ML/AqP7py83UwH3yrVu6CdQuUGXJBenpNOOumkk0466aSTTtqX/gsHDq/zisz2fQAAAABJRU5ErkJggg==",
		 url = "https://radio.wosy.ru/"
	 },
	 -- English : 
	 {
		 name = "Radio paradise",
		 logo = "https://images-eu.ssl-images-amazon.com/images/I/51A-N2vXAlL.png",
		 url = "https://stream.radioparadise.com/mp3-128"
	 },
	 {
		 name = "BBC radio",
		 logo = "https://images.radio.orange.com/radios/large_bbc_radio_1.png",
		 url = "http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio1_mf_p"
	 },
	 {
		 name = "Kiss",
		 logo = "https://i.iheart.com/v3/re/new_assets/cfe7bd3b-e0ec-40e9-8e2b-6ed7df4cc446",
		 url = "http://broadcast.infomaniak.net/kisswestcoast-128.mp3"
	 },
	 {
		 name = "ONE HIP HOP",
		 logo = "https://i.imgur.com/qsUYZdX.png",
		 url = "http://listen.one.hiphop/live.mp3"
	 },
	 {
		 name = "HITS Radio",
		 logo = "https://i.imgur.com/X9c3RKz.png",
		 url = "https://16803.live.streamtheworld.com/977_JAMZ.mp3"
	 },
	 {
		 name = "Star 983 FM",
		 logo = "https://i.imgur.com/foRPZZQ.png",
		 url = "https://www.streamvortex.com:8444/s/11010"
	 },
 
 
 }
 
 --[[	
	 List of available light colors :
	 ["white"] = Color( 255, 255, 255, 255 ),
	 ["green"] = Color( 0, 255, 157, 255 ),
	 ["darkgreen"] = Color( 63, 127, 79, 255 ),
	 ["lightblue"] = Color( 0, 255, 255, 255 ),
	 ["blue"] = Color( 0, 161, 255, 255 ),
	 ["hardblue"] = Color( 0, 127, 127, 255 ),
	 ["darkblue"] = Color( 0, 31, 127, 255 ),
	 ["orange"] = Color( 255, 191, 0, 255 ),
	 ["darkorange"] = Color( 255, 93, 0, 255 ),
	 ["purple"] = Color( 127, 0, 255, 255 ),
	 ["darkpurple"] = Color( 63, 0, 127, 255 ),
	 ["red"] = Color( 255, 0, 0, 255 ),
	 ["darkred"] = Color( 127, 0, 0, 255 ),
	 ["pink"] = Color( 255, 0, 97, 255 ),
	 ["yellow"] = Color( 255, 229, 0, 255),
	 You can use "zero" on lights to make that the is no light.
 
	 The Secondary color should be one of these :
	 gold
	 silver
	 bronze
 ]]
 
 RetroBoombox.Config.Boombox = {
	 [ "boombox_starter" ] = {
		 MainColor = "darkblue",
		 SecondaryColor = "darkblue",
		 MainLightsColor = "white",
		 TubeLightsColor = "white",
		 SoundLightsColor = "purple",
		 ScreenBackgroundColor = Color( 90, 100, 200, 255 ),
		 ScreenContentColor = Color( 255, 255, 255, 255 ),
	 },
 
 
 }