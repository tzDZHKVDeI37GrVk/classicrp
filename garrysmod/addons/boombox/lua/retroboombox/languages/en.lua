return {
	["AlreadyCarryBoombox"] = "You already carry a boombox.",
	["TurnPower"] = "Turn ON/OFF",
	["Frequency"] = "Frequency",
	["Drop"] = "Drop",
	["Volume"] = "Volume",
	["LMB"] = "LMB",
	["RMB"] = "RMB",
}