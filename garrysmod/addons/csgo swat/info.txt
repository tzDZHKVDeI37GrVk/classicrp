"AddonInfo"
{
	"name" "CS:GO SWAT and FBI"
	"author_name" "author"
	"info" "This addon includes the Counter-Strike: Global Offensive FBI and SWAT ragdolls.
In order to make them live, download the Model Manipulator tool, spawn a combine soldier equipped with a CS:S weapon, and set the model of the npc. :)

Ported from CS:GO by Alexandrovich. Models by Valve."
}